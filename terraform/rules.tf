#Ruleset
resource "checkpoint_management_access_rule" "rule1" {
  layer = "${checkpoint_management_package.TFStrict.name} Network"
  position = {top = "top"}
  name = "Access to Management"
  enabled = true
  destination = ["CP-MGMT-MikeDomain"]
  destination_negate = false
  service_negate = false
  action = "Accept"
  action_settings = {
    enable_identity_captive_portal = false
  }

}


resource "checkpoint_management_access_rule" "rule2" {
  layer = "${checkpoint_management_package.TFStrict.name} Network"
  position = {below = checkpoint_management_access_rule.rule1.id}
  name = "Management to Network"
  enabled = true
  source = ["CP-FW1"]
 
  destination_negate = false
  service_negate = false
  action = "Accept"
  action_settings = {
    enable_identity_captive_portal = false
  }

}

